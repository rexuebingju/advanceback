import Vue from 'vue'
import Login from '../../src/components/login.vue'
import {readyConfig} from './config'

readyConfig(()=>{
    new Vue({
        el: '#login-main',
        components:{
            Login
        },
        methods:{
            login(data){
                location.href = 'index.html'
            }
        },
        data(){
            return{
                logo: require('../image/logo.JPG'),
                rules:{
                    name:{
                        required: true
                    },
                    password:{
                        required: true
                    }
                },
                messages:{
                    name:{
                        required: '请输入用户名'
                    },
                    password:{
                        required: '请输入密码'
                    }
                }
            }
        }
    })

})
