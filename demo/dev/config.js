import {IO} from '../../src/util/io'

class Config{
    constructor(){
        this.config = {}

        this.configData = JSON.parse(window.localStorage.getItem('config'))
    }

    init(callback){
        let io = new IO({
            url: 'config.json'
        })

        io.get((res)=>{
            window.localStorage.setItem('config', JSON.stringify(res))

            window.config = res

            this.configData = res

            callback()
        })
    }

    getData(){
        return JSON.parse(window.localStorage.getItem('config'))
    }
}

const configInstance = new Config()

export const readyConfig = (callback)=>{
    configInstance.init(()=>{
        callback()
    })
}

export default ()=>{

    return JSON.parse(window.localStorage.getItem('config'))
} 