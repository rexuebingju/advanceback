##advanceback开发说明

项目是基于metronic（目录位于assets/下）样式开发的一套SPA（单页面应用）快速开发后台管理框架

阅读代码需了解一个MVVM的库-- Vue.js，请参考文档：[Vue.js](http://cn.vuejs.org/guide/)


[demo](http://gavinzhulei.oschina.io)

让我们开始快速开发吧：

1. `git clone https://git.oschina.net/gavinzhulei/advanceback.git` 克隆项目
2. `cd advanceback` 进入项目根目录
3. `npm install` 安装项目所依赖的包
4. `webpack --watch` 运行实时编译环境
5. `webpack-dev-server --content-base --host 127.0.0.1 --prot 9000` 启动项目热替换服务器
6. 访问 http://127.0.0.1:9000/demo 即可查看项目例子和文档 
7. 让我们开始进入代码旅途吧